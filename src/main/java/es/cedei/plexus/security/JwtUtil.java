package es.cedei.plexus.security;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

import es.cedei.plexus.db.UsuarioRepositorio;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtil {
	
	public static String KEYSECRET = "P@tit0";
	
    // Método para crear el JWT y enviarlo al cliente en el header de la respuesta
    static void addAuthentication(HttpServletResponse res, String email, String name) throws IOException {

        String token = Jwts.builder()
            .setSubject(email)
                
            // Vamos a asignar un tiempo de expiracion de 1 minuto
            // solo con fines demostrativos en el video que hay al final
            
            //.setExpiration(new Date(System.currentTimeMillis() + 6000000)) //Tiempo valido del token
            
            // Hash con el que firmaremos la clave
            .signWith(SignatureAlgorithm.HS512, KEYSECRET)
            .compact();



        System.out.println("TOKEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEN"+ token);
        //agremos el nombre del usuario al cuerpo
       
        StringBuilder json = new StringBuilder("{");
        json.append(String.format("\"name\": \"%s\",", name));
        json.append(String.format("\"token\": \"%s %s\"", "Bearer", token));
        json.append("}");
        
        //agregamos al encabezado el token
         res.addHeader("Authorization", "Bearer " + token);
         res.getWriter().write(json.toString());
         res.flushBuffer();
    }
    
 
    static Authentication getAuthentication(HttpServletRequest request, String[] roles) {
        String token = request.getHeader("Authorization");
        System.out.println("TOKEEEEEEEEEEEEEEEEEEEN"+token);
        if (token != null) {
            String user = Jwts.parser().setSigningKey(KEYSECRET).parseClaimsJws(token.replace("Bearer", "")).getBody()
                    .getSubject();
                    
            return user != null ? new UsernamePasswordAuthenticationToken(user, null, AuthorityUtils.createAuthorityList(roles)) : null;
        }
        return null;
    }
    
    static String getUser(HttpServletRequest request) {
        String user = null;
        String token = request.getHeader("Authorization");
        if (token != null) {
            user = Jwts.parser().setSigningKey(KEYSECRET).parseClaimsJws(token.replace("Bearer", "")).getBody().getSubject();
        }
        return user;
    }
}