package es.cedei.plexus.security;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import es.cedei.plexus.models.Usuario;

public class UserLogin extends Usuario implements UserDetails {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	UserLogin(Usuario user) {
		super();
		super.id = user.getId();
		super.name = user.getName();
		super.email = user.getEmail();
		super.password = user.getPassword();
		super.user_role = user.getUser_role();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return super.user_role.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return super.name;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	
}
