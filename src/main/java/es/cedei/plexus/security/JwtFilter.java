package es.cedei.plexus.security;

import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import es.cedei.plexus.db.UsuarioRepositorio;
import es.cedei.plexus.models.Usuario;
import io.jsonwebtoken.Jwts;

/**
 * Las peticiones que no sean /login pasarán por este filtro el cuál se encarga
 * de pasar el "request" a nuestra clase de utilidad JwtUtil para que valide el
 * token.
 */
public class JwtFilter extends GenericFilterBean {

	private UsuarioRepositorio repo;

	JwtFilter(UsuarioRepositorio rep) {
		this.repo = rep;
	}

	// @Override
	// public void doFilter(ServletRequest request, ServletResponse response,
	// FilterChain chain)
	// throws IOException, ServletException {
	// Authentication auth = JwtUtil.getAuthentication((HttpServletRequest)
	// request);
	// SecurityContextHolder.getContext().setAuthentication(auth);
	// chain.doFilter(request, response);
	// }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;  

		String[] roles = null;
		if(!req.getRequestURI().equals("/api/user/register")){
			String user = JwtUtil.getUser((HttpServletRequest) request);
			Usuario aux = this.repo.findByEmail(user);
			roles = aux.getUser_role().stream().map(role -> role.getName()).collect(Collectors.toList())
			.toArray(new String[aux.getUser_role().size()]);
		}
		Authentication auth = JwtUtil.getAuthentication((HttpServletRequest) request, roles);
		SecurityContextHolder.getContext().setAuthentication(auth);
		chain.doFilter(request, response);


	}


}
