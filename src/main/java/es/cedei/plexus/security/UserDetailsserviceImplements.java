package es.cedei.plexus.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import es.cedei.plexus.db.RolesRepositorio;
import es.cedei.plexus.db.UsuarioRepositorio;
import es.cedei.plexus.models.Roles;
import es.cedei.plexus.models.Usuario;

/**
 * UserDetailsserviceImplements
 */
@Service
public class UserDetailsserviceImplements implements UserDetailsService {

    @Autowired
    private UsuarioRepositorio userRepo;
    @Autowired
    private RolesRepositorio rolesRepo;
    
    public UserDetailsserviceImplements(UsuarioRepositorio usuarioRepository) {
		this.userRepo = usuarioRepository;
	}

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario user = userRepo.findByEmail(username);
        if(user == null){
            throw new UsernameNotFoundException(username);
        }
        //return new User(user.getEmail(),user.getPassword(),true,true,true,true,new ArrayList<>());
        return new UserLogin(user);
	}
    
    
//    private boolean hasRole(String role) {
//    	  Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
//    	  SecurityContextHolder.getContext().getAuthentication().getAuthorities();
//    	  boolean hasRole = false;
//    	  for (GrantedAuthority authority : authorities) {
//    	     hasRole = authority.getAuthority().equals(role);
//    	     if (hasRole) {
//    		  break;
//    	     }
//    	  }
//    	  return hasRole;
//    	}  
    
//    private Usuario findUserbyUername(String username) {
//        if(username.equalsIgnoreCase("admin")) {
//          return new Usuario(username, "password", "administrador");
//        }
//        return null;
//      }
    
    
//    public User registerNewUserAccount(UserDto accountDto) throws EmailExistsException {
//    	  
//        if (emailExist(accountDto.getEmail())) {
//            throw new EmailExistsException
//              ("There is an account with that email adress: " + accountDto.getEmail());
//        }
//        User user = new User();
//     
//        user.setFirstName(accountDto.getFirstName());
//        user.setLastName(accountDto.getLastName());
//        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
//        user.setEmail(accountDto.getEmail());
//     
//        user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
//        return repository.save(user);
//    }
    
    
//   private List<GrantedAuthority> getUserRolesList(List<String> roles) {
//	           List<GrantedAuthority> authorities = new ArrayList<>();
//        for (String rol : roles) {
//            authorities.add(new SimpleGrantedAuthority(rol));
//        }
//        return authorities;
// }
//
//    private Collection<? extends GrantedAuthority> getUserRolesColect(
//    		  Collection<Roles> roles) {
//    		    List<GrantedAuthority> authorities = new ArrayList<>();
//    		    for (Roles rol: roles) {
//    		        authorities.add(new SimpleGrantedAuthority(rol.getName()));
//    		        rol.getLista_privilegios().stream()
//    		         .map(p -> new SimpleGrantedAuthority(p.getName()))
//    		         .forEach(authorities::add);
//    		    }
//    		     
//    		    return authorities;
//    		}
}