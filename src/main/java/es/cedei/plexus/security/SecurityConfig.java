package es.cedei.plexus.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import es.cedei.plexus.db.UsuarioRepositorio;;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private UserDetailsserviceImplements userDetailsService;

	@Autowired
	UsuarioRepositorio repo;

	public SecurityConfig(UserDetailsserviceImplements userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// FUNCIONA SIN ROLES
		http
				.cors().and().csrf().disable()


				.authorizeRequests().antMatchers("/api/user/register").permitAll().and() // Registro

				.authorizeRequests().antMatchers(HttpMethod.DELETE, "/api/**").hasAnyAuthority("administrador")// Borrar
				.and().authorizeRequests().antMatchers(HttpMethod.POST, "/api/**").hasAnyAuthority("administrador")// Añadir
				.and().authorizeRequests().antMatchers(HttpMethod.GET, "/api/**")
				.hasAnyAuthority("usuario", "administrador")// Obtener
				.and().authorizeRequests().antMatchers(HttpMethod.PUT, "/api/**")
				.hasAnyAuthority("administrador", "encargado")// Actualizar
				.anyRequest().authenticated() // Resto de las rutas una ves logueado
				.and()
				.addFilterBefore(new LoginFilter("/login", authenticationManager()),UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(new JwtFilter(repo), UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}
	 
    //  public FilterRegistrationBean corsFilter() {
    //      UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    //      CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
    //      source.registerCorsConfiguration("/**", config);
    //      FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
    //      bean.setOrder(0);
    //      return bean;
	//  }
	
	// @Bean(name="corsConfigurationSource")
	// public CorsConfigurationSource corsFilter() {
	// 	UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	// 	CorsConfiguration config = new CorsConfiguration();
	// 	config.setAllowCredentials(true);
	// 	config.addAllowedOrigin("http://localhost:8082");
	// 	config.addAllowedHeader("*");
	// 	config.addAllowedMethod("*");
	// 	source.registerCorsConfiguration("/**", config);
	// 	//FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
	// 	return source;
	// }

}
