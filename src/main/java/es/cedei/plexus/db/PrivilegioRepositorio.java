/**
 * Creado 23/10/18
 * @Author : Noé Campos
 */
package es.cedei.plexus.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cedei.plexus.models.Privilegios;

/** Interfaz JPA de Privilegios */
@Repository
public interface PrivilegioRepositorio extends JpaRepository <Privilegios, Integer>{

}
