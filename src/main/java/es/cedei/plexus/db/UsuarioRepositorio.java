/**
 * Creado 23/10/18
 * Author: Noé Campos
 */
package es.cedei.plexus.db;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cedei.plexus.models.Usuario;

//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete

/** Interfaz JPA de usuarios */
@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario, Integer> {
	public Usuario findByName(String name);
    public Usuario findByNameAndEmailAndPassword(String name,String email,String password);
    public Usuario findByEmail(String email);
    


    //Collection<Usuario> getAllUsers();
//    public Usuario findByUserName(String username) {
//    	List<User> users = new ArrayList<User>();
//
//		users = sessionFactory.getCurrentSession()
//			.createQuery("from User where username=?")
//			.setParameter(0, username)
//			.list();
//
//		if (users.size() > 0) {
//			return users.get(0);
//		} else {
//			return null;
//		}
//    }
}
