package es.cedei.plexus.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.cedei.plexus.db.RolesRepositorio;
import es.cedei.plexus.db.UsuarioRepositorio;
import es.cedei.plexus.email.EmailService;
import es.cedei.plexus.exceptions.AddException;
import es.cedei.plexus.exceptions.DeleteException;
import es.cedei.plexus.exceptions.ListarException;
import es.cedei.plexus.exceptions.UpdateException;
import es.cedei.plexus.models.Roles;
import es.cedei.plexus.models.Usuario;
import es.cedei.plexus.utils.GeneratePassword;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/user")
public class UsuarioController {

	public String GMAIL = "susosapito@gmail.com";

	final Logger logger = LoggerFactory.getLogger(UsuarioController.class);


	@Autowired
	private UsuarioRepositorio usuarioRepositorio;

	@Autowired
	private RolesRepositorio RolesRepositorio;

	@Autowired
	private PasswordEncoder PasswordEncoder;

	@Autowired
	EmailService email;
	// @GetMapping(path = "/users")
	// public List<Usuario> getUsers(){
	// return Arrays.asList(new Usuario(1,"Paco","asd","asd"), new
	// Usuario(2,"Pedro","",""), new Usuario(3, "Juan","",""));
	// }

	/** Metodo get obtener Usuarios */

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al obtener lista de usuarios"),
			@ApiResponse(code = 401, message = "Sin autorización para ver el recurso"),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@ApiOperation(value = "Obtener todos los usuarios", response = Usuario.class)
	@GetMapping("/alluser") // Obtener todos los usuarios
	public ResponseEntity<?> getAllUsers() {
		ResponseEntity<?> response = null;
		List<Usuario> users = usuarioRepositorio.findAll();
		if (users != null) {
			response = new ResponseEntity<List<Usuario>>(users, HttpStatus.OK);
		} else {
			response = new ResponseEntity<String>("Error al leer todos los usuarios", HttpStatus.NOT_FOUND);
		}
		return response;
	}

	/** Metodo put actualizar Usuario */
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al actualizar usuario"),
			@ApiResponse(code = 201, message = "Sin autorización para actualizar usuario"),
			@ApiResponse(code = 401, message = "Acceso no autorizado al recurso actualizar usuario"),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso eliminar usuario"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@ApiOperation(value = "Actualizar los resultados de usuarios", response = Usuario.class)
	// @CrossOrigin(origins = "*")
	@PutMapping("/updateuser") // Actualizar usuario por id
	public ResponseEntity<?> updateUser(RequestEntity<Usuario> user) {
		ResponseEntity<?> response = null;
		logger.debug("entrando a depurar");
		try {
			System.out.println("UPDATEEEEEEEEEEEEEEEEEE" + user.getBody());
			UpdateEmptyBody(user.getBody());
			Usuario n = user.getBody();
			n.setPassword(PasswordEncoder.encode(user.getBody().getPassword()));
			response = new ResponseEntity<Usuario>(usuarioRepositorio.save(n), HttpStatus.OK);
		} catch (UpdateException e) {
			// TODO: handle exception
			response = new ResponseEntity<UpdateException>(new UpdateException(), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	/** Metodo delete borrar Usuario */
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al borrar un usuario"),
			@ApiResponse(code = 204, message = "Sin autorización para eliminar usuario"),
			@ApiResponse(code = 401, message = "Acceso prohibido al recurso eliminar usuario"),
			@ApiResponse(code = 403, message = "Resultado no encontrado") })
	@ApiOperation(value = "Eliminar los resultados de usuarios", response = Usuario.class)
	@DeleteMapping("/deleteuser/{id}") // Borrar usuario por id
	public ResponseEntity<?> deleteUser(@PathVariable Integer id) {
		ResponseEntity<?> response = null;
		try {
			DeleteEmptyBody(id);
			Integer del_user_id = id;
			usuarioRepositorio.deleteById(del_user_id);
			response = new ResponseEntity<String>("Usuario eliminado correctamente", HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			response = new ResponseEntity<DeleteException>(new DeleteException(), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	/** Metodo post añadir Usuario */
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al añadir un usuario"),
			@ApiResponse(code = 201, message = "Usuario creado"),
			@ApiResponse(code = 401, message = "Sin autorización para añadir usuario"),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso añadir usuario"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@ApiOperation(value = "añadir los resultados de usuarios", response = Usuario.class)
	@PostMapping("/adduser") // Añadir un nuevo usuario
	public ResponseEntity<?> addNewUser(RequestEntity<Usuario> user) {
		ResponseEntity<?> response = null;
		try {
			AddEmptyBody(user.getBody());
			Usuario nuevo_user = user.getBody();
			nuevo_user.setPassword(PasswordEncoder.encode(user.getBody().getPassword()));
			response = new ResponseEntity<Usuario>(usuarioRepositorio.save(nuevo_user), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			response = new ResponseEntity<AddException>(new AddException(), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al buscar un usuario"),
			@ApiResponse(code = 401, message = "Sin autorización para buscar usuario"),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso buscar usuario"),
			@ApiResponse(code = 404, message = "Usuario no encontrado") })
	@ApiOperation(value = "Busca un usuario por su identificador", response = Usuario.class)
	@GetMapping("/finduser/{id}")
	public ResponseEntity<?> findUserById(@PathVariable Integer id) {
		ResponseEntity<?> response = null;
		try {
			response = new ResponseEntity<Usuario>(this.usuarioRepositorio.findById(id).get(), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<ListarException>(new ListarException(), HttpStatus.NOT_FOUND);
		}
		return response;
	}

	/** Metodo post registrar Usuario */
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al añadir un usuario"),
			@ApiResponse(code = 201, message = "Usuario creado"),
			@ApiResponse(code = 401, message = "Sin autorización para añadir usuario"),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso añadir usuario"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@ApiOperation(value = "añadir los resultados de usuarios", response = Usuario.class)
	@PostMapping("/register") // Añadir un nuevo usuario
	public ResponseEntity<?> registerNewUser(RequestEntity<Usuario> user) {
		ResponseEntity<?> response = null;
		try {
			List<Roles> roles = RolesRepositorio.findAll(); // Obtengo todos los roles
			AddEmptyBody(user.getBody());
			Usuario nuevo_user = user.getBody();
			String nom = nuevo_user.getName();
			GeneratePassword p = new GeneratePassword();
			String passsincrifrar = p.generarPassword(8);
			nuevo_user.setPassword(PasswordEncoder.encode(passsincrifrar));
			nuevo_user.setUser_role(new ArrayList<Roles>(Arrays.asList(new Roles[]{roles.get(0)})));
			String mail = nuevo_user.getEmail();
			String pass = nuevo_user.getPassword();
			email.sendMail(nom, passsincrifrar, mail, GMAIL, GMAIL);
			response = new ResponseEntity<Usuario>(usuarioRepositorio.save(nuevo_user), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			response = new ResponseEntity<AddException>(new AddException(), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	// /** Metodo Exception para listar si el body es vacio */
	private void ListarEmptyBody(Object p) throws ListarException {
		if (p == null) {
			throw new ListarException();
		}
	}

	/** Metodo Exception para actualizar si el body es vacio */
	private void UpdateEmptyBody(Object p) throws UpdateException {
		if (p == null) {
			throw new UpdateException();
		}
	}

	/** Metodo Exception para borrar si el body es vacio */
	private void DeleteEmptyBody(Object p) throws DeleteException {
		if (p == null) {
			throw new DeleteException();
		}
	}

	/** Metodo Exception para añadir si el body es vacio */
	private void AddEmptyBody(Object p) throws AddException {
		if (p == null) {
			throw new AddException();
		}
	}

}