package es.cedei.plexus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.cedei.plexus.db.PrivilegioRepositorio;
import es.cedei.plexus.exceptions.AddException;
import es.cedei.plexus.exceptions.DeleteException;
import es.cedei.plexus.exceptions.ListarException;
import es.cedei.plexus.exceptions.UpdateException;
import es.cedei.plexus.models.Privilegios;
import es.cedei.plexus.models.Roles;
import es.cedei.plexus.models.Usuario;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/privilegios")
public class PrivilegiosController {

    @Autowired
    private PrivilegioRepositorio privilegioRepositorio;

    /** Metodo get obtener Privilegios */
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al obtener la lista de privilegios"),
            @ApiResponse(code = 401, message = "Sin autorización para ver el recurso de privilegios"),
            @ApiResponse(code = 403, message = "Acceso prohibido al recurso privilegios"),
            @ApiResponse(code = 404, message = "Resultado no encontrado") })
    @ApiOperation(value = "Obtener todos los privilegios", response = Privilegios.class, responseContainer = "List")
    @GetMapping("/allprivilegios") // Obtener todos los roles
    public ResponseEntity<?> getAllPrivilegios() {
        ResponseEntity<?> response = null;
        List<Privilegios> privilegios = privilegioRepositorio.findAll();
        if (privilegios != null) {
            response = new ResponseEntity<List<Privilegios>>(privilegios, HttpStatus.OK);
        } else {
            response = new ResponseEntity<String>("Error al leer todos los Privilegios", HttpStatus.NOT_FOUND);
        }
        return response;
    }


  	/** Metodo post añadir Roles con privilegios */
 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al añadir un rol con privilegio"),
 			@ApiResponse(code = 201, message = "Rol con privilegio creado"),
 			@ApiResponse(code = 401, message = "Sin autorización para añadir rol con privilegio"),
 			@ApiResponse(code = 403, message = "Acceso prohibido al recurso añadir rol con privilegio"),
 			@ApiResponse(code = 404, message = "Resultado no encontrado") })
 	@ApiOperation(value = "añadir los resultados de roles con privilegios", response = Privilegios.class, responseContainer = "List")
	@PostMapping("/addprivilegio/rol") // Añadir un nuevo rol a un privilegio
	public ResponseEntity<?> addNewRolePrivilegio(RequestEntity<Privilegios> user) {
 		ResponseEntity<?> response = null;
 		try {
 			AddEmptyBody(user.getBody());
 			Privilegios nuevo_privileg = user.getBody();
 			response = new ResponseEntity<Privilegios>(privilegioRepositorio.save(nuevo_privileg), HttpStatus.OK);
 		} catch (Exception e) {
 			// TODO: handle exception
 			response = new ResponseEntity<AddException>(new AddException(), HttpStatus.BAD_REQUEST);
 		}
		return response;
     }


 	/** Metodo post añadir Privilegio */
 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al añadir un privilegio"),
 			@ApiResponse(code = 201, message = "Privilegio creado"),
 			@ApiResponse(code = 401, message = "Sin autorización para añadir privilegio"),
 			@ApiResponse(code = 403, message = "Acceso prohibido al recurso añadir privilegio"),
 			@ApiResponse(code = 404, message = "Resultado no encontrado") })
 	@ApiOperation(value = "añadir los resultados de privilegios", response = Privilegios.class)
 	@PostMapping("/addprivilegio") // Añadir un nuevo privilegio
 	public ResponseEntity<?> addNewPrivilegio(RequestEntity<Privilegios> privileg) {
 		ResponseEntity<?> response = null;
 		try {
 			AddEmptyBody(privileg.getBody());
 			Privilegios nuevo_privileg = privileg.getBody();
 			response = new ResponseEntity<Privilegios>(privilegioRepositorio.save(nuevo_privileg), HttpStatus.OK);
 		} catch (Exception e) {
 			// TODO: handle exception
 			response = new ResponseEntity<AddException>(new AddException(), HttpStatus.BAD_REQUEST);
 		}
 		return response;
     }
     
 	/** Buscar privilegrio por id*/
 	@GetMapping("/findprivilege/{id}")
 	public ResponseEntity<?> findById(@PathVariable Integer id){
 		ResponseEntity<?> response = null;
 		try {
			response = new ResponseEntity<Privilegios>(this.privilegioRepositorio.findById(id).get(),HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<ListarException>(new ListarException(), HttpStatus.NOT_FOUND);
		}
 		return response;
 	}
 	
	/** Metodo delete borrar privilegios */
 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al borrar un privilegio"),
 			@ApiResponse(code = 204, message = "Sin autorización para eliminar privilegio"),
 			@ApiResponse(code = 401, message = "Acceso prohibido al recurso eliminar privilegio"),
 			@ApiResponse(code = 403, message = "Resultado no encontrado") })
 	@ApiOperation(value = "Eliminar los resultados de privilegrios", response = Privilegios.class)
 	@DeleteMapping("/deleteprivilegio/{id}") // Borrar privilegio por id
 	public ResponseEntity<?> deletePrivilegios(@PathVariable Integer id) {
 		ResponseEntity<?> response = null;
 		try {
 			DeleteEmptyBody(id);
 			Privilegios r = privilegioRepositorio.findById(id).get();
 			r.getRole_privilege().clear();
 			/*
 			
 			if(r.getRole_privilege().size() > 0 ) {
 				for (Roles usu : r.getRole_privilege()) {
 					usu.getLista_privilegios().removeIf(e -> e.equals(r));
				}
 			}*/
 			Integer del_privileg_id = id;
 			privilegioRepositorio.deleteById(del_privileg_id);
 			response = new ResponseEntity<String>("Privilegio eliminado correctamente", HttpStatus.OK);
 		} catch (Exception e) {
 			// TODO: handle exception
 			response = new ResponseEntity<DeleteException>(new DeleteException(), HttpStatus.BAD_REQUEST);
 		}
 		return response;
     }
     

 	/** Metodo put actualizar Privilegio */

 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al actualizar privilegios"),
 			@ApiResponse(code = 201, message = "Sin autorización para actualizar privilegios"),
 			@ApiResponse(code = 401, message = "Acceso no autorizado al recurso actualizar privilegios"),
 			@ApiResponse(code = 403, message = "Acceso prohibido al recurso actualizar privilegio"),
 			@ApiResponse(code = 404, message = "Resultado no encontrado") })
 	@ApiOperation(value = "Actualizar los resultados de privilegios", response = Privilegios.class)
 	@PutMapping("/updateprivilegio") // Actualizar usuario por id
 	public ResponseEntity<?> updatePrivilegios(RequestEntity<Privilegios> privileg) {
 		ResponseEntity<?> response = null;
 		try {
 			UpdateEmptyBody(privileg.getBody());
 			Privilegios n = privileg.getBody();
 			response = new ResponseEntity<Privilegios>(privilegioRepositorio.save(n), HttpStatus.OK);
 		} catch (Exception e) {
 			// TODO: handle exception
 			response = new ResponseEntity<UpdateException>(new UpdateException(), HttpStatus.BAD_REQUEST);
 		}
 		return response;
 	}
     

     // 	/** Metodo Exception para listar si el body es vacio */
 	private void ListarEmptyBody(Object p) throws ListarException {
 		if (p == null) {
 			throw new ListarException();
         }
    }
 	
 	/** Metodo Exception para actualizar si el body es vacio */
 	private void UpdateEmptyBody(Object p) throws UpdateException {
 		if (p == null) {
 			throw new UpdateException();
 		}
    }

 	/** Metodo Exception para borrar si el body es vacio */
 	private void DeleteEmptyBody(Object p) throws DeleteException {
 		if (p == null) {
 			throw new DeleteException();
 		}
    }

 	/** Metodo Exception para añadir si el body es vacio */
 	private void AddEmptyBody(Object p) throws AddException {
 		if (p == null) {
 			throw new AddException();
 		}
 	}

}