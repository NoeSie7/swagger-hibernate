package es.cedei.plexus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.cedei.plexus.exceptions.AddException;
import es.cedei.plexus.exceptions.DeleteException;
import es.cedei.plexus.exceptions.ListarException;
import es.cedei.plexus.exceptions.UpdateException;
import es.cedei.plexus.models.Privilegios;
import es.cedei.plexus.models.Roles;
import es.cedei.plexus.models.Usuario;
import es.cedei.plexus.db.RolesRepositorio;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/roles")
public class RolesController {
    @Autowired
     private RolesRepositorio rolesRepositorio;
     
    	/** Metodo get obtener Roles */
 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al obtener la lista de roles"),
 			@ApiResponse(code = 401, message = "Sin autorización para ver el recurso de roles"),
 			@ApiResponse(code = 403, message = "Acceso prohibido al recurso roles"),
 			@ApiResponse(code = 404, message = "Resultado no encontrado") })
 	@ApiOperation(value = "Obtener todos los roles", response = Roles.class, responseContainer = "List")
 	@GetMapping("/allroles") // Obtener todos los roles
 	public ResponseEntity<?> getAllRoles() {
 		ResponseEntity<?> response = null;
 		List<Roles> roles = rolesRepositorio.findAll();
 		if (roles != null) {
 			response = new ResponseEntity<List<Roles>>(roles, HttpStatus.OK);
 		} else {
 			response = new ResponseEntity<String>("Error al leer todos los roles", HttpStatus.NOT_FOUND);
 		}
 		return response;
     }
     
      	/** Metodo put actualizar Rol */
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al actualizar rol"),
			@ApiResponse(code = 201, message = "Sin autorización para actualizar rol"),
			@ApiResponse(code = 401, message = "Acceso no autorizado al recurso actualizar rol"),
			@ApiResponse(code = 403, message = "Acceso prohibido al recurso actualizar rol"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@ApiOperation(value = "Actualizar los resultados de roles", response = Roles.class)
	@PutMapping("/updaterol") // Actualizar usuario por id
	public ResponseEntity<?> updateRol(RequestEntity<Roles> rol) {
		ResponseEntity<?> response = null;
		try {
			UpdateEmptyBody(rol.getBody());
			Roles rol_update_body = rol.getBody();
			response = new ResponseEntity<Roles>(rolesRepositorio.save(rol_update_body), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			response = new ResponseEntity<UpdateException>(new UpdateException(), HttpStatus.BAD_REQUEST);
		}
		return response;
    }
    

     	/** Metodo delete borrar Roles */
 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al borrar un rol"),
 			@ApiResponse(code = 204, message = "Sin autorización para eliminar un rol"),
 			@ApiResponse(code = 401, message = "Acceso prohibido al recurso eliminar rol"),
 			@ApiResponse(code = 403, message = "Resultado no encontrado") })
 	@ApiOperation(value = "Eliminar los resultados de roles", response = Roles.class)
 	@DeleteMapping("/deleterol/{id}") // Borrar privilegio por id
 	public ResponseEntity<?> deleteRoles(@PathVariable Integer id) {
 		ResponseEntity<?> response = null;
 		try {
 			DeleteEmptyBody(id);
 			Roles r = rolesRepositorio.findById(id).get();
 			if(r.getLista_usuarios().size() > 0 ) {	
 				for (Usuario usu : r.getLista_usuarios()) {
 					usu.getUser_role().removeIf(e -> e.equals(r));
				}
 			}
 			if(r.getLista_privilegios().size() > 0) {
 				for (Privilegios aux : r.getLista_privilegios()) {
					aux.getRole_privilege().removeIf(e -> e.equals(r));
				}
 			}
 			Integer del_roles_id = id;
 			rolesRepositorio.deleteById(del_roles_id);
 			response = new ResponseEntity<String>("Rol eliminado correctamente", HttpStatus.OK);
 		} catch (Exception e) {
 			// TODO: handle exception
 			response = new ResponseEntity<String>("La peticion no es correcta al borrar Rol", HttpStatus.BAD_REQUEST);
 		}
 		return response;
 	}


 	@GetMapping("/findrol/{id}")
 	public ResponseEntity<?> findById(@PathVariable Integer id){
 		ResponseEntity<?> response = null;
 		try {
			response = new ResponseEntity<Roles>(this.rolesRepositorio.findById(id).get(),HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<ListarException>(new ListarException(), HttpStatus.NOT_FOUND);
		}
 		return response;
 	}
 	
     // 	/** Metodo post añadir Rol */
 	@ApiResponses(value = { @ApiResponse(code = 200, message = "Éxito al añadir un rol"),
 			@ApiResponse(code = 201, message = "Rol creado"),
 			@ApiResponse(code = 401, message = "Sin autorización para añadir rol"),
 			@ApiResponse(code = 403, message = "Acceso prohibido al recurso añadir roles"),
 			@ApiResponse(code = 404, message = "Resultado no encontrado") })
 	@ApiOperation(value = "añadir los resultados de roles", response = Roles.class)
 	@PostMapping("/addrol") // Añadir un nuevo usuario
 	public ResponseEntity<?> addNewRole(RequestEntity<Roles> rol) {
 		ResponseEntity<?> response = null;
 		try {
 			AddEmptyBody(rol.getBody());
 			Roles nuevo_rol = rol.getBody();
 			response = new ResponseEntity<Roles>(rolesRepositorio.save(nuevo_rol), HttpStatus.OK);
 		} catch (Exception e) {
 			// TODO: handle exception
 			response = new ResponseEntity<AddException>(new AddException(), HttpStatus.BAD_REQUEST);
 		}
 		return response;
 	}

    // 	/** Metodo Exception para listar si el body es vacio */
 	private void ListarEmptyBody(Object p) throws ListarException {
        if (p == null) {
            throw new ListarException();
        }
   }
    
    /** Metodo Exception para actualizar si el body es vacio */
    private void UpdateEmptyBody(Object p) throws UpdateException {
        if (p == null) {
            throw new UpdateException();
        }
   }

    /** Metodo Exception para borrar si el body es vacio */
    private void DeleteEmptyBody(Object p) throws DeleteException {
        if (p == null) {
            throw new DeleteException();
        }
   }

    /** Metodo Exception para añadir si el body es vacio */
    private void AddEmptyBody(Object p) throws AddException {
        if (p == null) {
            throw new AddException();
        }
    }
}