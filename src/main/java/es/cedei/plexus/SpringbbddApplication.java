package es.cedei.plexus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SpringbbddApplication {

	public static void main(String[] args) {
		
//		final Logger logger = LoggerFactory.getLogger(SpringApplication.class);
//		SpringApplication.run(SpringApplication.class, args);
//		logger.debug("--Application Started--");
//		logger.trace("--TRACE--");

		SpringApplication.run(SpringbbddApplication.class, args);
	}
}
