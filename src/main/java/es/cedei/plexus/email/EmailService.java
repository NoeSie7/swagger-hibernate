package es.cedei.plexus.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class EmailService {
	
	@Autowired
	private TemplateEngine templateEngine;

	 @Autowired 
	 private JavaMailSender mailSender;

	    public String sendMail(String nom,String pass,String mail, String From, String To) {
			MimeMessagePreparator messagePreparator = mimeMessage ->{
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
				messageHelper.setFrom(From);
				messageHelper.setTo(To);
				messageHelper.setSubject("Usuario Registrado");
				messageHelper.setText(build(nom, pass, mail), true);
			};
	        try {
	            mailSender.send(messagePreparator);
	            return "Email enviado correctamente!";
	        } catch (Exception e) {
	            e.printStackTrace();
	            return "Error al enviar email";
	        }
		}
		
		public String build(String nom, String pass, String mail){
			Context context = new Context();
			context.setVariable("nombre", nom);
			context.setVariable("password", pass);
			context.setVariable("email", mail);
			return templateEngine.process("emailTemplate", context);
		}
}
