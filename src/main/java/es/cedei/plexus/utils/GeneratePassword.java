package es.cedei.plexus.utils;

import java.util.Arrays;
import java.util.Random;

/**
 * GeneratePassword
 */
public class GeneratePassword {

    private String letras;
    private String numeros;
    private String pass;
    private Random random;
    private String conjunto;

    public GeneratePassword() {
        this.letras = "abcdefghijklmnopqrstuvwxyz!$%&/-_";
        this.numeros = "0123456789";
        this.conjunto = this.letras + this.numeros;
        this.random = new Random();
    }


    public String generarPassword( int max) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < max; i++) {
            Integer index = random.nextInt(this.conjunto.length());
            res.append(this.conjunto.charAt(index));
        }
        return res.toString();
    }
}