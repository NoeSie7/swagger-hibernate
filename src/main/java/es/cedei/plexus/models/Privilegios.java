/**
 * Creado 23/10/18
 * @Author: Noé Campos
 */
package es.cedei.plexus.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/** Clase Privilegios */
@Entity
@Table(name="privilege")
public class Privilegios {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_privilege",unique= true , nullable = false)
	/**
	 * @param id_privilegio
	 */
	private Integer id_privilegio;
	
	@Column(name="name", length = 20)
	/**
	 * @param name
	 */
	private String name;
	
	//@ManyToMany( fetch = FetchType.LAZY)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "role_privilege", joinColumns = {@JoinColumn(name = "id_privilege")}, inverseJoinColumns = {@JoinColumn(name = "id_role")} )
	/**
	 * @param role_privilege
	 */
	public List<Roles> role_privilege = new ArrayList<>();


	public List<Roles> getRole_privilege() {
		return role_privilege;
	}

	public void setRole_privilege(List<Roles> role_privilege) {
		this.role_privilege = role_privilege;
	}

	/**
	 * Metodo obtener id privilegio
	 */
	public Integer getId_privilegio() {
		return id_privilegio;
	}

	/**
	 * Metodo asignar id privilegio
	 */
	public void setId_privilegio(Integer id_privilegio) {
		this.id_privilegio = id_privilegio;
	}

	/**
	 * Metodo obtener name privilegio
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metodo asignar name privilegio
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	
}
