/**
 * Creado 23/10/18
 * @Author: Noé Campos
 */
package es.cedei.plexus.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/** Clase Roles */
@Entity
@Table(name="role")
public class Roles {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_role",unique= true , nullable = false)
	/**
	 * @param id_rol
	 */
	private Integer id_rol;
	
	@Column(name="name", length = 20)
	/**
	 * @param name
	 */
	private String name;


	@ManyToMany(mappedBy = "user_role")
	@JsonIgnore
	/**
	 * @param lista_usuarios
	 */
	 private List<Usuario> lista_usuarios = new ArrayList<>();
	 

	 public List<Privilegios> getLista_privilegios() {
		return lista_privilegios;
	}

	public void setLista_privilegios(List<Privilegios> lista_privilegios) {
		this.lista_privilegios = lista_privilegios;
	}

	public List<Usuario> getLista_usuarios() {
		return lista_usuarios;
	}

	public void setLista_usuarios(List<Usuario> lista_usuarios) {
		this.lista_usuarios = lista_usuarios;
	}

	@ManyToMany(mappedBy = "role_privilege")
	@JsonIgnore
	/**
	 * @param lista_usuarios
	 */
	 private List<Privilegios> lista_privilegios = new ArrayList<>();
	 

	/**
	 * Metodo obtener id rol
	 */
	public Integer getId_rol() {
		return id_rol;
	}

	/**
	 * Metodo asignar id rol
	 */
	public void setId_rol(Integer id_rol) {
		this.id_rol = id_rol;
	}

	/**
	 * Metodo obtener name rol
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metodo asignar name rol
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
