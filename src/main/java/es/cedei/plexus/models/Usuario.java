/**
 * Creado 23/10/18
 * @Author Noé Campos
 */
package es.cedei.plexus.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
/**
 * Clase Usuario entidad que hibernate se encargara de traducir automaticamente
 * a una tabla
 */
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user", unique = true, nullable = false)
	/**
	 * @param id
	 */
	protected Integer id;

	@Column(name = "name", length = 20)
	/**
	 * @param name
	 */
	protected String name;

	@Column(name = "email", length = 20)
	/**
	 * @param email
	 */
	protected String email;
	
	@Column(name = "password", length = 20)
	/**
	 * @param password
	 */
	protected String password;

	

	@ManyToMany( fetch = FetchType.EAGER)
	//@ManyToMany
	@JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "id_user") }, inverseJoinColumns = {
			@JoinColumn(name = "id_role") })
	/**
	 * @param user_role
	 */
	protected List<Roles> user_role = new ArrayList<>();

	public List<Roles> getUser_role() {
		return user_role;
	}

	public void setUser_role(List<Roles> user_role) {
		this.user_role = user_role;
	}

	/** Constructor por defecto */
	public Usuario() {
		this.id = 0;
		this.name = null;
		this.email = null;
		this.password = null;
		this.user_role = new ArrayList<>();
	}

	/** Constructor */
	public Usuario(Integer id, String name, String email,String password, List<Roles> user_role) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.user_role = user_role;
	}

	/**
	 * Metodo Obteneer id usuario
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Metodo Asignar id usuario
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Metodo obtener name usuario
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metodo Asignar name usuario
	 */
	public void setName(String name) {
		if(name != null) {
		this.name = name;
		}
	}

	/**
	 * Metodo Obtener email usuario
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Metodo Asignar email usuario
	 */
	public void setEmail(String email) {
		if(email != null)
		this.email = email;
	}
	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
//	/**
//	 * Metodo Obtener lista roles de usuario
//	 */
//	public List<Roles> getLista_roles() {
//		return this.user_role;
//	}

	/**
	 * Metodo Asingar lista roles de usuario
	 */
	public void setLista_roles(List<Roles> user_role) {
		if (user_role == null || user_role.size() > 0) {
		this.user_role = user_role;
		}
	}

}
