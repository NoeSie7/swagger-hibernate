package es.cedei.plexus.exceptions;

public class ListarException extends Exception {

    public ListarException(){
        super("Error al listar");
    }
}