package es.cedei.plexus.exceptions;

public class AddException extends Exception {

    public AddException(){
        super("Error al añadir");
    }
}