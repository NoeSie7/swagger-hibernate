package es.cedei.plexus.exceptions;

public class UpdateException extends Exception {

    public UpdateException(){
        super("Error al actualizar");
    }
}