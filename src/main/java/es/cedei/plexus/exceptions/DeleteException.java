package es.cedei.plexus.exceptions;

public class DeleteException extends Exception {

    public DeleteException(){
        super("Error al borrar");
    }
}